@extends('coba')

@section('title', 'list')

@section('container')

<div class="container">
    <img src="{{ URL::asset('images/background.jpg') }}" alt="">
    <div class="teks2">
        <h1>Daftar list.</h1>
        @foreach ($list as $lists)

        <p><strong>List {{ $lists['list']}}</strong></p>
        <p> --- {{ $lists['subject']}}</p>

        @endforeach

    </div>
</div>

@endsection