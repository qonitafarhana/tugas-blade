<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class ListController extends Controller
{
    public function index()
    {

        $list = [
            ['list' => 'satu', 'subject' => 'Ini adalah list satu'],
            ['list' => 'dua', 'subject' => 'Ini adalah list dua'],
            ['list' => 'tiga', 'subject' => 'Ini adalah list tiga'],
        ];

        return view('list', compact('list'));
    }
}
